import * as axios from "axios";

const instance = axios.create({
  baseURL: "https://social-network.samuraijs.com/api/1.0/",
  withCredentials: true,
  headers: { "API-KEY": "43b047d5-c816-4ee5-bc65-7672a9bfbb41" },
});

export const friendsAndUsersAPI = {
  getFriendsAndUsers(currentPage = 1, pageSize = 5) {
    return instance
      .get(`users?page=${currentPage}&count=${pageSize}`)
      .then((response) => response.data);
  },
  toggleFollowUser(user) {
    if (user.followed) {
      return instance
        .delete(`follow/${user.id}`)
    }
    return instance
      .post(`https://social-network.samuraijs.com/api/1.0/follow/${user.id}`)
  },

};

export const profileAPI = {
  getProfileData(userId) {
    return instance.get(`profile/${userId}`);
  },
  getStatus(userId) {
    return instance.get(`profile/status/${userId}`)
  },
  updateStatus(status) {
    return instance.put(`profile/status/`, { status: status })
  }
}

export const authAPI = {
  authMe() {
    return instance.get(`auth/me`);
  },
  signIn(formData) {
    return instance.post(`/auth/login`, {...formData});
  },
  logOut() {
    return instance.delete(`/auth/login`);
  },
};
