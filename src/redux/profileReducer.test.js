import profileReducer, { addPost, deletePost } from "./profileReducer";

let state = {
    posts: [
      {
        id: 1,
        message: "H1, how are you?",
        likesCount: 0,
      },
      {
        id: 2,
        message: "It`s my first post",
        likesCount: 23,
      },
    ],
  };

it('length of posts should be incremented', () => {

    // 1. text data
    let action = addPost('newPost text bla bla bla');
    
    // 2. action
    const newState = profileReducer(state, action);
      // 3. expectation
    expect(newState.posts.length).toBe(3);
})
it('message of new post shiold be correct', () => {

    // 1. text data
    let action = addPost('newPost text bla bla bla');
    
    // 2. action
    const newState = profileReducer(state, action);
      // 3. expectation
    expect(newState.posts[0].message).toBe('newPost text bla bla bla');
})

it('chosen post have to be deleted', () => {

    // 1. text data
    let action = deletePost(2);
    
    // 2. action
    const newState = profileReducer(state, action);
      // 3. expectation
    expect(newState.posts.length).toBe(1);
})