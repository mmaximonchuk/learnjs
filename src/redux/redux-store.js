import { applyMiddleware, combineReducers, createStore } from "redux";
import profileReducer from "../redux/profileReducer";
import dialogsReducer from "../redux/dialogsReducer";
import findFriendsAndUsersReducer from "./findFriendsAndUsersReducer";
import thunkMiddleware from 'redux-thunk';
import sidebarReducer from "../redux/sidebarReducer";
import authReducer from "./auth-reducer";
import appReducer from "./app-reducer";
import {reducer as formReducer} from 'redux-form';

const reducersBatch = combineReducers({
  profilePage: profileReducer,
  dialogsPage: dialogsReducer,
  findFriendsandUsersPage: findFriendsAndUsersReducer,
  sidebar: sidebarReducer,
  auth: authReducer,
  app: appReducer,
  form: formReducer,
});

let store = createStore(reducersBatch, applyMiddleware(thunkMiddleware));

window.store = store;

export default store;
