import { stopSubmit } from "redux-form";
import { authAPI } from "../api/api";
/* eslint-disable default-case */
const SET_USER_DATA = "auth/SET-USER-DATA";
const SET_IS_LOADING = "auth/SET-IS-LOADING";

let initialState = {
  id: null,
  email: null,
  login: null,
  isLoading: false,
  isAuth: false,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return {
        ...state,
        ...action.payload,
      };
    case SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.loadingState,
      };
    default:
      return state;
  }
};

export const setAuthUserData = (id, email, login, isAuth) => {
  return {
    type: SET_USER_DATA,
    payload: {
      id,
      email,
      login,
      isAuth,
    },
  };
};

export const setIsLoading = (loadingState) => {
  return {
    type: SET_IS_LOADING,
    loadingState,
  };
};

export const authThunk = () => async (dispatch) => {
  dispatch(setIsLoading(true));

  const response = await authAPI.authMe();

  if (response.data.resultCode === 0) {
    let { id, login, email } = response.data.data;
    dispatch(setAuthUserData(id, email, login, true));
  }
  
  dispatch(setIsLoading(false));
};

export const signInThunk = (formData) => async (dispatch) => {
  const response = await authAPI.signIn(formData);
  if (response.data.resultCode === 0) {
    dispatch(authThunk(response.data.data));
  } else {
    let messageError =
      response.data.messages.length > 0
        ? response.data.messages[0]
        : "Wrong credentials!";
    dispatch(stopSubmit("login", { _error: messageError }));
  }
};

export const logOutThunk = () => {
  return async (dispatch) => {
    const response = await authAPI.logOut();
    if (response.data.resultCode === 0) {
      dispatch(setAuthUserData(null, null, null, false));
    }
  };
};
export default authReducer;
