import { profileAPI } from "../api/api";
/* eslint-disable default-case */
const ADD_POST = "profile/ADD-POST";
const SET_USER_PROFILE = "profile/SET-USER-PROFILE";
const SET_STATUS = "profile/SET-STATUS";
const DELETE_POST = "profile/DELETE-POST";

let initialState = {
  posts: [
    {
      id: 1,
      message: "H1, how are you?",
      likesCount: 0,
    },
    {
      id: 2,
      message: "It`s my first post",
      likesCount: 23,
    },
  ],
  profile: null,
  status: "",
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST:
      return {
        ...state,
        posts: [
          {
            id: new Date().getMilliseconds(),
            message: action.newPostMessage,
            likesCount: 0,
          },
          ...state.posts,
        ],
      };
    case DELETE_POST:
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== action.postId),
      };
    case SET_USER_PROFILE:
      return {
        ...state,
        profile: action.profileData,
      };
    case SET_STATUS:
      return {
        ...state,
        status: action.newStatus,
      };
    default:
      return state;
  }
};

export const addPost = (newPostMessage) => {
  return {
    type: ADD_POST,
    newPostMessage,
  };
};
export const deletePost = (postId) => {
  return {
    type: DELETE_POST,
    postId,
  };
};
export const setUserProfile = (profileData) => {
  return {
    type: SET_USER_PROFILE,
    profileData,
  };
};
export const setUserStatus = (newStatus) => {
  return {
    type: SET_STATUS,
    newStatus,
  };
};

export const setProfileDataThunk = (userId) => {
  // if (!userId) userId = 13839;
  return async (dispatch) => {
    const response = await profileAPI.getProfileData(userId);

    dispatch(setUserProfile(response.data));
  };
};

export const setStatusThunk = (userId) => {
  // if (!userId) userId = 2;
  return async (dispatch) => {
    const response = await profileAPI.getStatus(userId);
    dispatch(setUserStatus(response.data));
  };
};

export const updateStatusThunk = (status) => {
  return async (dispatch) => {
    const response = await profileAPI.updateStatus(status);
    if (response.data.resultCode === 0) {
      dispatch(setUserStatus(status));
    }
  };
};

export default profileReducer;
