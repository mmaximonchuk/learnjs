const ADD_MESSAGE = 'dialogs/ADD-MESSAGE';

let initialState = {
    messages: [
        {
            message: 'Hi!',
            id: 1
        },
        {
            message: 'How are you?',
            id: 2
        },
        {
            message: 'Great!',
            id: 3
        },
        {
            message: 'YoYoYoYo',
            id: 4
        },       
      ],
      dialogs: [
        {
            name: 'Maxim',
            avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_iEXq5Tq8suPbnZd51iuXk2m7Q64Zide5WA&usqp=CAU',
            id: 1
        },
        {
            name: 'Andrey',
            avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_iEXq5Tq8suPbnZd51iuXk2m7Q64Zide5WA&usqp=CAU',
            id: 2
        },
        {
            name: 'Vadym',
            avatar: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTrl-dJqXo9rcikdLo4FHdnklzThVIim-6a8A&usqp=CAU',
            id: 3
        },
        {
            name: 'Nikita',
            avatar: '',
            id: 4
        },
        {
            name: 'Vlad',
            avatar: '',
            id: 5
        },
        {
            name: 'Yarik',
            avatar: '',
            id: 6
        },
      ],
}

const dialogsReducer = (state = initialState, action) => {
    switch(action.type) {
        case ADD_MESSAGE:
            return { 
                ...state,
                messages: [...state.messages, {id: new Date().getMilliseconds(), message: action.newMessageText, }],      
            };    
        default: 
            return state;
    }

}

export const addMessage = (newMessageText) => {
    return {
      type: ADD_MESSAGE,
      newMessageText,
    }
  }


export default dialogsReducer;
