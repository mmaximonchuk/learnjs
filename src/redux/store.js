import profileReducer from './profileReducer';
import dialogsReducer from './dialogsReducer';
import sidebarReducer from './sidebarReducer';
export const ADD_POST = 'ADD-POST';
export const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';

export const ADD_MESSAGE = 'ADD-MESSAGE';
export const UPDATE_NEW_MESSAGE_TEXT = 'UPDATE-NEW-MESSAGE-TEXT';

let store = {
    _state: {
        profilePage: {
            posts: [
                {
                  id: 1,
                  message: 'H1, how are you?',
                  likesCount: 0
                },
                {
                  id: 2,
                  message: 'It`s my first post',
                  likesCount: 23
                },
              ],
            newPostText: ''
        },
        dialogsPage: {
            messages: [
                {
                    message: 'Hi!',
                    id: 1
                },
                {
                    message: 'How are you?',
                    id: 2
                },
                {
                    message: 'Great!',
                    id: 3
                },
                {
                    message: 'YoYoYoYo',
                    id: 4
                },       
              ],
              newMessageText: '',
              dialogs: [
                {
                    name: 'Maxim',
                    avatar: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEXVKSz////VJyrVJSjUGh7UICPTEhfSAAv++vr99fXSBg7TDxTzy8vVJinTFRr66Oj44OD219jxwMHmj5DfaWvYPT/utrf99vbsrK3dXmDaR0nrpKXXNTjssbLifH700tLgcHLbVFbdW13WMTTlhojYS03pnJ5l6vEsAAAH5ElEQVR4nO2d2ZaiMBBAISFhiQINoqJIY2///4kD7SAoOyZAir4vPso9WaoqhERRoaPM/QDC+TOUn+kMDdN03LcM1zFNY7L/FW5ouN7Zj3fXQLF1Xbcy0l9bCa672D97rnBVkYbu3t8F1GK2RgjGCCkFCGFMiGYziwY7f+8KfApBhqbnhwqjGtmWvepAW6JRpoS+Z4p5FAGGxsa/4lSuy+3BM9XEV38joM/yNnQuScQ0PMTubok1FiUXh/MTcTU8HHeYku0Iu5wtoXh3PPB8KI6GXqLRQV2zoSkJ1RKP32PxMnT8yOKgl0takc+rt/Ix3IS69krnrLLV9HDD5dk4GBqXEyNc9W4QdrpwmFtfNjTOAcUC/DIwDc4vO75oaJwjYX43x+hVx9cMLxHlO/yqbGl0mc1wc2Ii2y8Hs9Mrc854Q+dDn8Lv11H/GB87xhoaPhUxfzZBqD92OI403AQ2r/DeD2QHI7vqKEMzFjqB1oNpPKq+GmO4+dSmbcAbSPsc04wjDL8mm2GewfrXBIbuyZ7JL8M+DV7wGGp4nCQENoPZUaihEbM5RmAZxOJhcWOQofOtzeyXoX0PCv9DDN+VKYN8M0R5F2O4n3kIFmC2F2H4o889BAuQ/sPfMLbm1nrAinkbJnRupydowtXQCOcM8/XYYb+o0cvwsEDBTLHXynEfw2UK9lXsYbjELnqjV0ftYbhYwUyRh2GyXMFUsXtG7TT8WlqYeIR2Voxdhj/LCvRVrK7spsNwv6BUrR6kd+So7YbvdOmCWcHYXmm0GjrKUqqJNrDSWi+2GRrfy6gHuyDfbWGxzTBeQkXfB62t0GgxPLK5n7w3bctTzYbu7ItO/UGseZGx2fAkwyyTg0/DDb+WnKxVsRtzmybDjT73Mw9Eb3qn0WBofsrURzPwZ8ObqQZDaQJFQVPIqDfcSJCtPYNofT+tNTQC2fpoBg5qU5taQ1+ueTTH9vsaOhL20QxE61LwOsMPORLuKuSjn6F0obCgLijWGEqVrj1Sl7xVDS/ylBRVWHUPXMXQiORtwrQRo0rEqBiel7162AU9dxkakejtlGLZVhrx2VDyJqxpxCdDOfO1MpXc7cnwInsTpo14aTWUOBbmPMfER8ONzLEwh21aDENZM9IyJGw2dOTNSMvoTqOhL9/aRR2a32goebTP2UZNht7S34b2xfIaDHcQ5pkMktQbHrh9Pzg3iBxqDY/y5zM59FhrCKaTpt10V2fojPoAe5kg7NQYAki6C0rpd2GYwOmkD7Pp3VD24v6RUql/NwRRVhQUBcbdEEhOmlPkpnfDq/y1bxl8fTaEFCsyiniRG3qQYkUG9Z4MgQ3D0kDMDUGsX5S5r2XkhgqsYZgOROXR0IUVDTPyjWD/DffQJpp0qtk/GIKbaIqp5r8hoNowJ68R/xsGkNLuG9ugbGhIur+kDUSNkqELZR2xjOWWDD14wSINF17J8CznNq927HPJEGCwuIeLm2EML1ik4SIuGQIMh/eAeDMEVuDf+F/m3wyl34FRBw4KQwNc7ZSBFONuaE58rNU0INssDGG8v39GLwyB7FB45rZj4dfQBWro3g3fICbeaer99mcoO3+G8lMyhD+Xwo+H8HMa+Hkp/NoCfn24ghof/joN/LU2+Oul8Ne84b+3gP/uCf77Q/jvgCEGxKf3+ADDxdNeDPj7aeDviYK/r20FexPBTTWV/aXw9wjD3+cNrcyv7tWHNhBrvreA/80M/O+e4H+7toLvDyHFi/pvSCHViPXfAa/gW2743+PD6aZNZyqs4FwM+GebQMlNW86nAbJjoeWMIRhrGW3nRMEoMFrP+oJ/XhuE9LvjzD345ybCP/tS+lK/+/xS2Rux+wzaFZwjDP8saKljYr/zvFdwJjv8c/VXcDcC/PstJM3dhtxRAv+emRXcFbSC+57kC4pD7+xawb1rkiVvY+7OW8H9h/DvsJQoZIy9h3QFd8nCvw94BXc6r+Be7hXcrZ7mNsteXaSNuUxvQzVZcvpmJ53P322ohstVtMPux+9haCxW0Q7bAmF/Q/WwUEU7PHQ/fC/DhSr2E+xnuMiO2quL9jZMZ9SlBQ3aPYsOM1TjZYV+q62cGGeo/iwogUN6VyYzxlDds6VUGph15KIjDdV3ZRn1IlHaq4nxhqrzvYSqX/turQdfMlSNePblKcTiflFinGG2PDXvYMRti05cDFX3NGfwt0/Ny4a8DNOKUZ+rGbHeWQ1yMVQ3n9ocoxFpn03vJngbqmZMp29GTOOGt0sCDNNmDCY+PAvZwZgGHG+oGj6dMvwT6g+LEa8bpuH/Y7IZB+sfg4I8J8O0q54mCY6YnUZ20JcNVfUSUdGbNbc0qu5Vm85QNc6R0GkV0+g8dgDyMcwcA2GOmAav+nEwTB0vJyZiXiXsdHnZj4thyibUNb4Dcqvp4SvzSwEfwzR2+JHF7ftFRKzIHx8fHuFlmOIlGuUgiQjVEq/77/rC0VBVD8cdpuSV7rolFO+OvVZ6+8LVMMW5JBHTRn0WjrDGouTCq3fm8DZMMTb+FVNtUIdFRKP46m84zJ3PCDDMMD0/VFique3yRNtUjimh742qjboRZPiLu/d3AbWYrRGCMSq7IoQxIZrNLBrs/P3gpYkBiDT8xXC9sx/vroFi67puZaS/thJcd7F/9lwB/fIR4YZ3DNN03LcM1zFN4WJ3pjOciz9D+YFv+A8sfn5LkmCeqwAAAABJRU5ErkJggg==',
                    id: 1
                },
                {
                    name: 'Andrey',
                    avatar: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEXVKSz////VJyrVJSjUGh7UICPTEhfSAAv++vr99fXSBg7TDxTzy8vVJinTFRr66Oj44OD219jxwMHmj5DfaWvYPT/utrf99vbsrK3dXmDaR0nrpKXXNTjssbLifH700tLgcHLbVFbdW13WMTTlhojYS03pnJ5l6vEsAAAH5ElEQVR4nO2d2ZaiMBBAISFhiQINoqJIY2///4kD7SAoOyZAir4vPso9WaoqhERRoaPM/QDC+TOUn+kMDdN03LcM1zFNY7L/FW5ouN7Zj3fXQLF1Xbcy0l9bCa672D97rnBVkYbu3t8F1GK2RgjGCCkFCGFMiGYziwY7f+8KfApBhqbnhwqjGtmWvepAW6JRpoS+Z4p5FAGGxsa/4lSuy+3BM9XEV38joM/yNnQuScQ0PMTubok1FiUXh/MTcTU8HHeYku0Iu5wtoXh3PPB8KI6GXqLRQV2zoSkJ1RKP32PxMnT8yOKgl0takc+rt/Ix3IS69krnrLLV9HDD5dk4GBqXEyNc9W4QdrpwmFtfNjTOAcUC/DIwDc4vO75oaJwjYX43x+hVx9cMLxHlO/yqbGl0mc1wc2Ii2y8Hs9Mrc854Q+dDn8Lv11H/GB87xhoaPhUxfzZBqD92OI403AQ2r/DeD2QHI7vqKEMzFjqB1oNpPKq+GmO4+dSmbcAbSPsc04wjDL8mm2GewfrXBIbuyZ7JL8M+DV7wGGp4nCQENoPZUaihEbM5RmAZxOJhcWOQofOtzeyXoX0PCv9DDN+VKYN8M0R5F2O4n3kIFmC2F2H4o889BAuQ/sPfMLbm1nrAinkbJnRupydowtXQCOcM8/XYYb+o0cvwsEDBTLHXynEfw2UK9lXsYbjELnqjV0ftYbhYwUyRh2GyXMFUsXtG7TT8WlqYeIR2Voxdhj/LCvRVrK7spsNwv6BUrR6kd+So7YbvdOmCWcHYXmm0GjrKUqqJNrDSWi+2GRrfy6gHuyDfbWGxzTBeQkXfB62t0GgxPLK5n7w3bctTzYbu7ItO/UGseZGx2fAkwyyTg0/DDb+WnKxVsRtzmybDjT73Mw9Eb3qn0WBofsrURzPwZ8ObqQZDaQJFQVPIqDfcSJCtPYNofT+tNTQC2fpoBg5qU5taQ1+ueTTH9vsaOhL20QxE61LwOsMPORLuKuSjn6F0obCgLijWGEqVrj1Sl7xVDS/ylBRVWHUPXMXQiORtwrQRo0rEqBiel7162AU9dxkakejtlGLZVhrx2VDyJqxpxCdDOfO1MpXc7cnwInsTpo14aTWUOBbmPMfER8ONzLEwh21aDENZM9IyJGw2dOTNSMvoTqOhL9/aRR2a32goebTP2UZNht7S34b2xfIaDHcQ5pkMktQbHrh9Pzg3iBxqDY/y5zM59FhrCKaTpt10V2fojPoAe5kg7NQYAki6C0rpd2GYwOmkD7Pp3VD24v6RUql/NwRRVhQUBcbdEEhOmlPkpnfDq/y1bxl8fTaEFCsyiniRG3qQYkUG9Z4MgQ3D0kDMDUGsX5S5r2XkhgqsYZgOROXR0IUVDTPyjWD/DffQJpp0qtk/GIKbaIqp5r8hoNowJ68R/xsGkNLuG9ugbGhIur+kDUSNkqELZR2xjOWWDD14wSINF17J8CznNq927HPJEGCwuIeLm2EML1ik4SIuGQIMh/eAeDMEVuDf+F/m3wyl34FRBw4KQwNc7ZSBFONuaE58rNU0INssDGG8v39GLwyB7FB45rZj4dfQBWro3g3fICbeaer99mcoO3+G8lMyhD+Xwo+H8HMa+Hkp/NoCfn24ghof/joN/LU2+Oul8Ne84b+3gP/uCf77Q/jvgCEGxKf3+ADDxdNeDPj7aeDviYK/r20FexPBTTWV/aXw9wjD3+cNrcyv7tWHNhBrvreA/80M/O+e4H+7toLvDyHFi/pvSCHViPXfAa/gW2743+PD6aZNZyqs4FwM+GebQMlNW86nAbJjoeWMIRhrGW3nRMEoMFrP+oJ/XhuE9LvjzD345ybCP/tS+lK/+/xS2Rux+wzaFZwjDP8saKljYr/zvFdwJjv8c/VXcDcC/PstJM3dhtxRAv+emRXcFbSC+57kC4pD7+xawb1rkiVvY+7OW8H9h/DvsJQoZIy9h3QFd8nCvw94BXc6r+Be7hXcrZ7mNsteXaSNuUxvQzVZcvpmJ53P322ohstVtMPux+9haCxW0Q7bAmF/Q/WwUEU7PHQ/fC/DhSr2E+xnuMiO2quL9jZMZ9SlBQ3aPYsOM1TjZYV+q62cGGeo/iwogUN6VyYzxlDds6VUGph15KIjDdV3ZRn1IlHaq4nxhqrzvYSqX/turQdfMlSNePblKcTiflFinGG2PDXvYMRti05cDFX3NGfwt0/Ny4a8DNOKUZ+rGbHeWQ1yMVQ3n9ocoxFpn03vJngbqmZMp29GTOOGt0sCDNNmDCY+PAvZwZgGHG+oGj6dMvwT6g+LEa8bpuH/Y7IZB+sfg4I8J8O0q54mCY6YnUZ20JcNVfUSUdGbNbc0qu5Vm85QNc6R0GkV0+g8dgDyMcwcA2GOmAav+nEwTB0vJyZiXiXsdHnZj4thyibUNb4Dcqvp4SvzSwEfwzR2+JHF7ftFRKzIHx8fHuFlmOIlGuUgiQjVEq/77/rC0VBVD8cdpuSV7rolFO+OvVZ6+8LVMMW5JBHTRn0WjrDGouTCq3fm8DZMMTb+FVNtUIdFRKP46m84zJ3PCDDMMD0/VFique3yRNtUjimh742qjboRZPiLu/d3AbWYrRGCMSq7IoQxIZrNLBrs/P3gpYkBiDT8xXC9sx/vroFi67puZaS/thJcd7F/9lwB/fIR4YZ3DNN03LcM1zFN4WJ3pjOciz9D+YFv+A8sfn5LkmCeqwAAAABJRU5ErkJggg==',
                    id: 2
                },
                {
                    name: 'Vadym',
                    avatar: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAdVBMVEXVKSz////VJyrVJSjUGh7UICPTEhfSAAv++vr99fXSBg7TDxTzy8vVJinTFRr66Oj44OD219jxwMHmj5DfaWvYPT/utrf99vbsrK3dXmDaR0nrpKXXNTjssbLifH700tLgcHLbVFbdW13WMTTlhojYS03pnJ5l6vEsAAAH5ElEQVR4nO2d2ZaiMBBAISFhiQINoqJIY2///4kD7SAoOyZAir4vPso9WaoqhERRoaPM/QDC+TOUn+kMDdN03LcM1zFNY7L/FW5ouN7Zj3fXQLF1Xbcy0l9bCa672D97rnBVkYbu3t8F1GK2RgjGCCkFCGFMiGYziwY7f+8KfApBhqbnhwqjGtmWvepAW6JRpoS+Z4p5FAGGxsa/4lSuy+3BM9XEV38joM/yNnQuScQ0PMTubok1FiUXh/MTcTU8HHeYku0Iu5wtoXh3PPB8KI6GXqLRQV2zoSkJ1RKP32PxMnT8yOKgl0takc+rt/Ix3IS69krnrLLV9HDD5dk4GBqXEyNc9W4QdrpwmFtfNjTOAcUC/DIwDc4vO75oaJwjYX43x+hVx9cMLxHlO/yqbGl0mc1wc2Ii2y8Hs9Mrc854Q+dDn8Lv11H/GB87xhoaPhUxfzZBqD92OI403AQ2r/DeD2QHI7vqKEMzFjqB1oNpPKq+GmO4+dSmbcAbSPsc04wjDL8mm2GewfrXBIbuyZ7JL8M+DV7wGGp4nCQENoPZUaihEbM5RmAZxOJhcWOQofOtzeyXoX0PCv9DDN+VKYN8M0R5F2O4n3kIFmC2F2H4o889BAuQ/sPfMLbm1nrAinkbJnRupydowtXQCOcM8/XYYb+o0cvwsEDBTLHXynEfw2UK9lXsYbjELnqjV0ftYbhYwUyRh2GyXMFUsXtG7TT8WlqYeIR2Voxdhj/LCvRVrK7spsNwv6BUrR6kd+So7YbvdOmCWcHYXmm0GjrKUqqJNrDSWi+2GRrfy6gHuyDfbWGxzTBeQkXfB62t0GgxPLK5n7w3bctTzYbu7ItO/UGseZGx2fAkwyyTg0/DDb+WnKxVsRtzmybDjT73Mw9Eb3qn0WBofsrURzPwZ8ObqQZDaQJFQVPIqDfcSJCtPYNofT+tNTQC2fpoBg5qU5taQ1+ueTTH9vsaOhL20QxE61LwOsMPORLuKuSjn6F0obCgLijWGEqVrj1Sl7xVDS/ylBRVWHUPXMXQiORtwrQRo0rEqBiel7162AU9dxkakejtlGLZVhrx2VDyJqxpxCdDOfO1MpXc7cnwInsTpo14aTWUOBbmPMfER8ONzLEwh21aDENZM9IyJGw2dOTNSMvoTqOhL9/aRR2a32goebTP2UZNht7S34b2xfIaDHcQ5pkMktQbHrh9Pzg3iBxqDY/y5zM59FhrCKaTpt10V2fojPoAe5kg7NQYAki6C0rpd2GYwOmkD7Pp3VD24v6RUql/NwRRVhQUBcbdEEhOmlPkpnfDq/y1bxl8fTaEFCsyiniRG3qQYkUG9Z4MgQ3D0kDMDUGsX5S5r2XkhgqsYZgOROXR0IUVDTPyjWD/DffQJpp0qtk/GIKbaIqp5r8hoNowJ68R/xsGkNLuG9ugbGhIur+kDUSNkqELZR2xjOWWDD14wSINF17J8CznNq927HPJEGCwuIeLm2EML1ik4SIuGQIMh/eAeDMEVuDf+F/m3wyl34FRBw4KQwNc7ZSBFONuaE58rNU0INssDGG8v39GLwyB7FB45rZj4dfQBWro3g3fICbeaer99mcoO3+G8lMyhD+Xwo+H8HMa+Hkp/NoCfn24ghof/joN/LU2+Oul8Ne84b+3gP/uCf77Q/jvgCEGxKf3+ADDxdNeDPj7aeDviYK/r20FexPBTTWV/aXw9wjD3+cNrcyv7tWHNhBrvreA/80M/O+e4H+7toLvDyHFi/pvSCHViPXfAa/gW2743+PD6aZNZyqs4FwM+GebQMlNW86nAbJjoeWMIRhrGW3nRMEoMFrP+oJ/XhuE9LvjzD345ybCP/tS+lK/+/xS2Rux+wzaFZwjDP8saKljYr/zvFdwJjv8c/VXcDcC/PstJM3dhtxRAv+emRXcFbSC+57kC4pD7+xawb1rkiVvY+7OW8H9h/DvsJQoZIy9h3QFd8nCvw94BXc6r+Be7hXcrZ7mNsteXaSNuUxvQzVZcvpmJ53P322ohstVtMPux+9haCxW0Q7bAmF/Q/WwUEU7PHQ/fC/DhSr2E+xnuMiO2quL9jZMZ9SlBQ3aPYsOM1TjZYV+q62cGGeo/iwogUN6VyYzxlDds6VUGph15KIjDdV3ZRn1IlHaq4nxhqrzvYSqX/turQdfMlSNePblKcTiflFinGG2PDXvYMRti05cDFX3NGfwt0/Ny4a8DNOKUZ+rGbHeWQ1yMVQ3n9ocoxFpn03vJngbqmZMp29GTOOGt0sCDNNmDCY+PAvZwZgGHG+oGj6dMvwT6g+LEa8bpuH/Y7IZB+sfg4I8J8O0q54mCY6YnUZ20JcNVfUSUdGbNbc0qu5Vm85QNc6R0GkV0+g8dgDyMcwcA2GOmAav+nEwTB0vJyZiXiXsdHnZj4thyibUNb4Dcqvp4SvzSwEfwzR2+JHF7ftFRKzIHx8fHuFlmOIlGuUgiQjVEq/77/rC0VBVD8cdpuSV7rolFO+OvVZ6+8LVMMW5JBHTRn0WjrDGouTCq3fm8DZMMTb+FVNtUIdFRKP46m84zJ3PCDDMMD0/VFique3yRNtUjimh742qjboRZPiLu/d3AbWYrRGCMSq7IoQxIZrNLBrs/P3gpYkBiDT8xXC9sx/vroFi67puZaS/thJcd7F/9lwB/fIR4YZ3DNN03LcM1zFN4WJ3pjOciz9D+YFv+A8sfn5LkmCeqwAAAABJRU5ErkJggg==',
                    id: 3
                },
                {
                    name: 'Nikita',
                    avatar: '',
                    id: 4
                },
                {
                    name: 'Vlad',
                    avatar: '',
                    id: 5
                },
                {
                    name: 'Yarik',
                    avatar: '',
                    id: 6
                },
              ],
        },
        sidebar: '', 
    },
    _callSubscriber() {
        console.log('State was changed!');
    },

    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },

    dispatch(action) { // action - { type: 'ADD-POST'}

    this._state.profilePage = profileReducer(this._state.profilePage, action);
    this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
    this._state.sidebar = sidebarReducer(this._state.sidebar, action);

    this._callSubscriber(this._state);
    },
}





export default store;
// store - OOP