import { authThunk } from "./auth-reducer";

const SET_INITIALIZED = "app/SET-INITIALIZED";

const initialState = {
  isInitialized: false,
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_INITIALIZED:
      return {
        ...state,
        isInitialized: true,
      };
    default:
      return state;
  }
};

export const setInitializedSucsess = () => {
  return {
    type: SET_INITIALIZED,
  };
};

export const initializeApp = () => (dispatch) => {
  let resultPromise = dispatch(authThunk());
  Promise.all([resultPromise]).then(() => {
    dispatch(setInitializedSucsess());
  });
};

export default appReducer;
