// import { createSelector } from "reselect"

export const getUsersState = (state) => {
    return state.findFriendsandUsersPage.friendsAndUsers
}
// export const getUsersStateSuperSelector = createSelector(getUsersState, (users) => {
//     return users.filter(user => true)
// })
export const getPageSizeState = (state) => {
    return state.findFriendsandUsersPage.pageSize
}
export const getTotalUsersCountState = (state) => {
    return state.findFriendsandUsersPage.totalUsersCount
}
export const getCurrentPageState = (state) => {
    return state.findFriendsandUsersPage.currentPage
}
export const getIsLoadingState = (state) => {
    return state.findFriendsandUsersPage.isLoading
}
export const getFollowingProgressState = (state) => {
    return state.findFriendsandUsersPage.followingProgress
}
export const getArrayOfCurrentPagesState = (state) => {
    return state.findFriendsandUsersPage.arrayOfCurrentPages
}