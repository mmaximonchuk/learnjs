import { friendsAndUsersAPI } from "../api/api";
const SET_USERS = "friends-and-users/SET-USERS";
const TOGGLE_FOLLOW = "friends-and-users/TOGGLE-FOLLOW";
const FOLLOWING_PROGRESS = "friends-and-users/FOLLOWING-PROGRESS";
const CHANGE_CURRENT_PAGE = "friends-and-users/CHANGE-CURRENT-PAGE";
const SET_TOTAL_USERS_COUNT = "friends-and-users/SET-TOTAL-USERS-COUNT";
const SET_LOADING_STATE = "friends-and-users/SET-LOADING-STATE";
const SET_NUMBER_OF_PAGES = "friends-and-users/SET-NUMBER-OF-PAGES";

let initialState = {
  friendsAndUsers: [
    // {
    //   id: 1,
    //   avatarUrl: '',
    //   followed: false,
    //   fullName: 'Maxim',
    //   status: 'I`m no hero never was never will be',
    //   location: {
    //     city: 'Kyiv',
    //     country: 'Ukraine',
    //   },
    // },
    // {
    //   id: 2,
    //   avatarUrl: '',
    //   fullName: 'Vadim',
    //   followed: true,
    //   status: 'Tech Lead',
    //   location: {
    //     city: 'Dubai',
    //     country: 'OAE',
    //   },
    // },
    // {
    //   id: 3,
    //   avatarUrl: '',
    //   fullName: 'Andrew',
    //   followed: false,
    //   status: 'Team Lead',
    //   location: {
    //     city: 'Warsaw',
    //     country: 'Poland',
    //   },
    // },
  ],
  pageSize: 5,
  totalUsersCount: 0,
  currentPage: 1,
  arrayOfCurrentPages: [],
  isLoading: false,
  followingProgress: [],
};

const findFriendsAndUsersReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FOLLOW:
      return {
        ...state,
        friendsAndUsers: state.friendsAndUsers.map((user) => {
          if (user.id === action.userId) {
            return { ...user, followed: !user.followed };
          }
          return user;
        }),
      };
    case SET_USERS: {
      let actionBody =
        action.friendsAndUsers === undefined || null
          ? ""
          : action.friendsAndUsers;
      return {
        ...state,
        friendsAndUsers: [...actionBody],
      };
    }
    case CHANGE_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.pageNumber,
      };
    case SET_TOTAL_USERS_COUNT:
      return {
        ...state,
        totalUsersCount: action.usersCount,
      };
    case SET_LOADING_STATE:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case FOLLOWING_PROGRESS:
      return {
        ...state,
        followingProgress: action.followingProgress
          ? [...state.followingProgress, action.userId]
          : [state.followingProgress.filter((id) => id !== action.userId)],
      };
    case SET_NUMBER_OF_PAGES:
      return {
        ...state,
        arrayOfCurrentPages: [...action.pages],
      };
    default:
      return state;
  }
};

export const setFriendsAndUsers = (friendsAndUsers) => ({
  type: SET_USERS,
  friendsAndUsers,
});
export const toggleFollow = (userId) => ({ type: TOGGLE_FOLLOW, userId });
export const changeCurrentPage = (pageNumber) => ({
  type: CHANGE_CURRENT_PAGE,
  pageNumber,
});
export const setTotalUsersCount = (usersCount) => ({
  type: SET_TOTAL_USERS_COUNT,
  usersCount,
});
export const setLoadingState = (isLoading) => ({
  type: SET_LOADING_STATE,
  isLoading,
});
export const setFollowingProgress = (followingProgress, userId) => ({
  type: FOLLOWING_PROGRESS,
  followingProgress,
  userId,
});
export const setNumberOfPages = (pages) => ({
  type: SET_NUMBER_OF_PAGES,
  pages,
});

export const getFriendsAndUsersThunk = (
  currentPage,
  pageSize,
  setPaginator
) => {
  return async (dispatch) => {
    dispatch(setLoadingState(true));

    const response = await friendsAndUsersAPI.getFriendsAndUsers(
      currentPage,
      pageSize
    );

    dispatch(setLoadingState(false));
    dispatch(setFriendsAndUsers(response.items));
    dispatch(setTotalUsersCount(response.totalCount));
    setPaginator(currentPage, response.totalCount, pageSize);
  };
};
export const toggleFollowUserThunk = (user) => {
  return async (dispatch) => {
    dispatch(setFollowingProgress(true, user.id));
    const response = await friendsAndUsersAPI.toggleFollowUser(
      user,
      setFollowingProgress
    );
    if (response.data !== undefined && response.data.resultCode === 0) {
      dispatch(toggleFollow(user.id));
    }
    dispatch(setFollowingProgress(false, user.id));
  };
};

export default findFriendsAndUsersReducer;
