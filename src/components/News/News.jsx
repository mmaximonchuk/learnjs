import React from "react";
import TextLoop from "react-text-loop";

import "./news.scss";

const News = () => {
  return (
    <div className="news">
    <div className="loop-container">
        <TextLoop>
            <span className="loop-item">First new </span>
            <span className="loop-item">Second new </span>
            <span className="loop-item">Third new </span>
        </TextLoop>
        all of them soon will be available
    </div>
      
    </div>
  );
};

export default News;
