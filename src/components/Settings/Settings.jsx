import React from "react";
import "./settings.css";
import withAuthRedirect from "../../HOC/WithAuthRedirect";
import { compose } from "redux";

const Settings = () => {
  return <div>Settings</div>;
};

export default compose(withAuthRedirect)(Settings);
