import React from "react";
import { Field, reduxForm } from "redux-form";
import Post from "./Post/Post";
import { Textarea } from "../../common/FormControls/FormControls";
import "./MyPosts.scss";
import { required, maxLength } from "../../../utils/validators";
import { CSSTransition, TransitionGroup } from "react-transition-group";
const maxLength10 = maxLength(20);

const MyPosts = React.memo(({posts, addPost}) => {
  const onSubmit = (postMessage) => {
    // console.log(postMessage.postMessage);
    addPost(postMessage.postMessage);
  };

  return (
    <div className="content__main">
      <div className="posts">
        <h3 className="my-posts">My posts</h3>
        <ReduxPostForm onSubmit={onSubmit} />
        <TransitionGroup component="div" className="posts__wrapper">
          {posts.map((post) => (
            <CSSTransition
              key={post.id}
              timeout={800}
              classNames={"post"}
            >
              <Post
                key={post.id}
                message={post.message}
                likesCount={post.likesCount}
              />
            </CSSTransition>
          ))}
        </TransitionGroup>
      </div>
    </div>
  );
});
// class MyPosts extends React.PureComponent {

//   // shouldComponentUpdate(nextProps, nextState) {
//   //   return nextProps !== this.props || nextState !== this.state
//   // }

//   onSubmit = (postMessage) => {
//     console.log(postMessage.postMessage);
//     this.props.addPost(postMessage.postMessage);
//   };

//   render() {
//     return (
//     <div className="content__main">
//       <div className="posts">
//         <h3 className="my-posts">My posts</h3>
//         <ReduxPostForm onSubmit={this.onSubmit} />
//         <div className="posts__wrapper">
//           {this.props.posts.map((post) => (
//             <Post
//               key={post.id}
//               message={post.message}
//               likesCount={post.likesCount}
//             />
//           ))}
//         </div>
//       </div>
//     </div>
//   )}
// };

const PostForm = ({ handleSubmit }) => {
  return (
    <form onSubmit={handleSubmit} className="new-posts">
      <Field
        name="postMessage"
        placeholder="New post..."
        component={Textarea}
        validate={[required, maxLength10]}
      />
      <button type="submit">Add post</button>
    </form>
  );
};

const ReduxPostForm = reduxForm({ form: "posts" })(PostForm);

export default MyPosts;
