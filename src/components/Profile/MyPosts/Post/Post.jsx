import React from "react";
import iconLike from "../../../../images/profile/post/heart-f.svg";
import "./Post.css";

const Post = ({message, likesCount}) => {
  return (
    <div className="post">
      <div className="ava">
        <img
          className="person-avatar"
          src="https://download-cs.net/steam/avatars/3412.jpg"
          alt="ava"
        />
        <div className="likes-count">
          <img src={iconLike} alt="" /> {likesCount}
        </div>
      </div>
      <p className="post__content">{message}</p>
    </div>
  );
};

export default Post;
