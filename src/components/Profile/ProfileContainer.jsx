import React from "react";
import Profile from "./Profile";
import { connect } from "react-redux";
import withAuthRedirect from "../../HOC/WithAuthRedirect";
import { setProfileDataThunk, setStatusThunk, updateStatusThunk } from "../../redux/profileReducer.js";
import { withRouter } from "react-router-dom";
import { compose } from "redux";
import { useEffect } from "react";

// class ProfileContainer extends React.Component {
//   componentDidMount() {
//     let userId = this.props.match.params.userId;
//     if (!userId) {
//       userId = this.props.userId
//       this.redirectToLogin()
//     };
//     this.props.setProfileDataThunk(userId);
//     this.props.setStatusThunk(userId);
//   }

//   componentDidUpdate() {
//     this.redirectToLogin()
//   }

//   redirectToLogin() {
//     if(!this.props.userId && window.location.pathname === '/profile') {
//       this.props.history.push('/login');
//     }
//   }

//   render() {
//     return <Profile {...this.props} />;
//   }
// }

const ProfileContainer = (props) => {

  useEffect(() => {
    let userId = props.match.params.userId;
    if (!userId) {
      userId = props.userId
      redirectToLogin()
    };
    props.setProfileDataThunk(userId);
    props.setStatusThunk(userId);
  }, [props.isAuth, props.match.url])


  function redirectToLogin() {
    if(!props.userId && props.match.url === '/profile') {
      props.history.push('/login');
    }
  }

  return <Profile {...props} />;
}

const mapStateToProps = (state) => ({
  profile: state.profilePage.profile,
  userId: state.auth.id,
  login: state.auth.login,
  status: state.profilePage.status,
  isAuth: state.auth.isAuth,
});

export default compose(
  connect(mapStateToProps, { setProfileDataThunk, setStatusThunk, updateStatusThunk}),
  withRouter,
  // withAuthRedirect
)(ProfileContainer);
