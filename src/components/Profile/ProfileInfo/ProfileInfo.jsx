import React, { useState } from "react";
import { TransitionGroup, CSSTransition } from "react-transition-group";
import imgTemplateAvatar from "../../../images/dialogs/avatars/TemplateGreen.jpg";
import PreLoader from "../../common/PreLoader";
import Status from "./Status";
import { Field, reduxForm } from "redux-form";
import "./profileInfo.scss";
import ProfileTriangle from "../../common/ProfileTriangle";
import { Input } from "../../common/FormControls/FormControls";
// import { maxLength } from "../../../utils/validators";

const ProfileInfo = ({ profile, updateStatus, status }) => {
  const [detailsOpen, setDetailsOpen] = useState(false);
  if (!profile) {
    return <PreLoader />;
  }

  const onSubmit = (formData) => {};
  return (
    <>
      {/* <div className="slider">
        <img
          className="main-pic"
          src="https://images.unsplash.com/photo-1447710441604-5bdc41bc6517?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
          alt=""
        />
      </div> */}
      <div className="description-block">
        <div className="user-avatar__container">
          <img
            src={profile.photos.large || imgTemplateAvatar}
            alt=""
            className="avatar"
          />
        </div>
        <div className="user-bio">
          <h2 className="user__fullname">{profile.fullName}</h2>
          <Status updateStatus={updateStatus} status={status} />
          <p className="user__looking-for-a-job">
            {`В поисках работы: ${profile.lookingForAJob}` || "Не указано"}
          </p>
        </div>
      </div>

      <div className={`details ${detailsOpen ? "active" : ""}`}>
        {detailsOpen ? (
          <div className={`details__wrapper`}>
            <p className="aboutMe">aboutMe</p>
            <ContactsReduxForm onSubmit={onSubmit} />
            <p className="looking-for-a-job">looking-for-a-job</p>
            <p className="looking-for-a-job__description">
              looking-for-a-job__description
            </p>
          </div>
        ) : (
          ""
        )}

        <button
          onClick={() => setDetailsOpen(!detailsOpen)}
          className={`details__trigger ${detailsOpen ? "active" : ""}`}
        >
          Click to see more details
          <ProfileTriangle className="details__triangle" />
        </button>
      </div>
    </>
  );
};

const ProfileContactsForm = ({ handleSubmit }) => {
  return (
    <form onSubmit={handleSubmit} className="contacts">
      <div className="contacts__item">
        <img src="" alt="" />
        <span>Fb</span>
        <Field
          className="facebook"
          name="facebook"
          placeholder="Login"
          type="text"
          component={Input}
        />
      </div>
      <div className="contacts__item">
        <img src="" alt="" />
        <span>Website</span>
        <Field
          className="website"
          name="website"
          placeholder="Login"
          type="text"
          component={Input}
        />
      </div>
      <div className="contacts__item">
        <img src="" alt="" />
        <span>GitHub</span>
        <Field
          className="gitHub"
          name="gitHub"
          placeholder="Login"
          type="text"
          component={Input}
        />
      </div>
      <div className="contacts__item">
        <img src="" alt="" />
        <span>Instagram</span>
        <Field
          className="instagram"
          name="instagram"
          placeholder="Login"
          type="text"
          component={Input}
        />
      </div>
      <div className="contacts__item">
        <img src="" alt="" />
        <span>Twitter</span>
        <Field
          className="twitter"
          name="twitter"
          placeholder="Login"
          type="text"
          component={Input}
        />
      </div>
      <div className="contacts__item">
        <img src="" alt="" />
        <span>vk</span>
        <Field
          className="vk"
          name="vk"
          placeholder="Login"
          type="text"
          component={Input}
        />
      </div>
      <div className="contacts__item">
        <img src="" alt="" />
        <span>youtube</span>
        <Field
          className="youtube"
          name="youtube"
          placeholder="Login"
          type="text"
          component={Input}
        />
      </div>
    </form>
  );
};

const ContactsReduxForm = reduxForm({
  form: "contacts",
})(ProfileContactsForm);

export default ProfileInfo;
