import React, { useEffect, useState } from "react";

const Status = (props) => {
const [editMode, setEditMode] = useState(false);
const [status, setStatus] = useState(props.status);

const activateStatusHandler = () => {
    setEditMode(true)
}
const deactivateStatusHandler = () => {
    setEditMode(false)
    props.updateStatus(status);
}
const changeStatusHandler = (e) => {
    setStatus(e.target.value);
}

useEffect(() => {
    setStatus(props.status);
}, [props.status])

    return (
        <>
          <div
            onDoubleClick={activateStatusHandler}
            className={`${!editMode ? "" : "hidden"} user__status`}
          >
            {props.status || "any status"}
          </div>
          {editMode && (
            <input
              autoFocus={true}
              onBlur={deactivateStatusHandler}
              type="text"
              className={`${
                editMode ? "" : "hidden"
              } change-status-area`}
              onChange={(e) => changeStatusHandler(e)}
              value={status}
            />
          )}
        </>
      );
}



export default Status;
