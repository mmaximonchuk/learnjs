import React from "react";
import "./profile.css";
import ProfileInfo from "./ProfileInfo/ProfileInfo";
import MyPostsContainer from "./MyPosts/MyPoststContainer";

const Profile = (props) => {

  return (
    <section className="profile">
      <ProfileInfo updateStatus={props.updateStatusThunk} status={props.status} profile={props.profile} />
      <MyPostsContainer store={props.store} />
    </section>
  );
};

export default Profile;
