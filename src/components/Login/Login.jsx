import React from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
// import { compose } from "redux";
import { Field, reduxForm } from "redux-form";
import { signInThunk, logOutThunk } from '../../redux/auth-reducer';
import { maxLength, required } from "../../utils/validators";
import { Input } from "../common/FormControls/FormControls";
import "./login.scss";

const maxLength40 = maxLength(40);

const Login = ({signInThunk, logOutThunk, isAuth}) => {
    const onSubmit = (formData) => {
        const captchaWithFormData = {...formData, captcha: true}
        console.log(captchaWithFormData);
        signInThunk(captchaWithFormData);
    }

    if(isAuth) return <Redirect to="/profile"/>

  return (
    <div className="login-page">
      <h1 className="login__title">Login</h1>
      <LoginReduxForm onSubmit={onSubmit} />
    </div>
  );
};

const LoginForm = ({handleSubmit, error}) => {
  return (
    <form onSubmit={handleSubmit} className="login-form">
      <label className="input__wrapper">
        <Field className="login-field" name="email" placeholder="Login" type="email" component={Input} validate={[required, maxLength40]} />
      </label>
      <label className="input__wrapper">
        <Field className="password-field" name="password" placeholder="Password" type="password" component={Input} validate={[required, maxLength40]}/>
      </label>
      <label className="input__wrapper">
        <Field name="rememberMe" type="checkbox" component="input" />
        <span>remember me</span>
      </label>
      {error && <p className="form__error">{error}</p>}
      <button type="submit" className="form__btn">
        Sign In
      </button>
    </form>
  );
};

const LoginReduxForm = reduxForm({
  form: "login",
})(LoginForm);

const mapStateToProps = (state) => ({
  isAuth: state.auth.isAuth,
})

const LoginFormContainer = connect(mapStateToProps, { signInThunk, logOutThunk })(Login)

export default LoginFormContainer;
