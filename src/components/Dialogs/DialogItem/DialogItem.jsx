import React from 'react';
import { NavLink } from 'react-router-dom';
import iconTamplateAvatar from '../../../images/dialogs/avatars/template.jfif';
import '../dialogs.css';


const DialogItem = ({avatar, name, id}) => {
    return (
        <div className={`dialog ` /*active*/}>
            <img className='dialog__avatar' src={avatar || iconTamplateAvatar} alt=""/>
            <NavLink to={`/dialogs/${id}`}>{name}</NavLink>  
        </div>
    )
}

export default DialogItem;