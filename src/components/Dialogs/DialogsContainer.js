import Dialogs from "./Dialogs";
import withAuthRedirect from "../../HOC/WithAuthRedirect";
import { addMessage } from "../../redux/dialogsReducer";
import { connect } from "react-redux";
import { compose } from "redux";

let mapStateToProps = (state) => {
  return {
    dialogsPage: state.dialogsPage,
  };
};

export default compose(
  connect(mapStateToProps, { addMessage }),
  withAuthRedirect
)(Dialogs);
