import React, { useState } from "react";
import DialogItem from "../Dialogs/DialogItem/DialogItem";
import Message from "../Dialogs/Message/Message";
import { Field, reduxForm } from "redux-form";
import iconSendMessage from "../../images/dialogs/input/arrow-circle-right-f.svg";
import "./dialogs.css";
import { Textarea } from "../common/FormControls/FormControls";
import { maxLength, required } from "../../utils/validators";



const maxMessageLength20 = maxLength(20);

const Dialogs = ({ dialogsPage, addMessage}) => {
  let state = dialogsPage;

  const onSubmit = (message) => {
    // console.log(message.messageField);
    addMessage(message.messageField);
  };

  return (
    <div className="dialogs">
      <div className="dialogs__items">
        {state.dialogs.map((dialog) => (
          <DialogItem
            key={dialog.id}
            name={dialog.name}
            avatar={dialog.avatar}
            id={dialog.id}
          />
        ))}
      </div>
      <div className="messages">
        {state.messages.map((message) => (
          <Message key={message.id} message={message.message} />
        ))}
        <ReduxMessage onSubmit={onSubmit} />
      </div>
    </div>
  );
};

const MessageInput = ({ handleSubmit }) => {
  return (
    <form onSubmit={handleSubmit} className="input-wrapper">
      <Field
        name="messageField"
        component={Textarea}
        validate={[required, maxMessageLength20]}
        placeholder="Enter your message..."
        className="input-message-field"
      />
      <button className="arrow" type="submit">
        <img src={iconSendMessage} alt="" />
      </button>
    </form>
  );
};

const ReduxMessage = reduxForm({
  form: "message",
})(MessageInput);

export default Dialogs;
