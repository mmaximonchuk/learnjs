import React from "react";
import { NavLink } from "react-router-dom";
import "./navbar.css";

const Navbar = () => {
  return (
    <nav className="nav">
      <div className="nav__links">
        <NavLink to="/profile">Profile</NavLink>{" "}
      </div>

      <div className="nav__links">
        <NavLink to="/dialogs">Messages</NavLink>
      </div>

      <div className="nav__links">
        <NavLink to="/news">News</NavLink>
      </div>

      <div className="nav__links">
        <NavLink to="/music">Music</NavLink>
      </div>

      <div className="nav__links">
        <NavLink to="/friends-and-users">Find Friends</NavLink>
      </div>

      <div className="nav__links">
        <NavLink to="/settings">Settings</NavLink>
      </div>
    </nav>
  );
};

export default Navbar;
