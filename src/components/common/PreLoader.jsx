import React from 'react';

const preLoader = ({initialize = false}) => {
    return (
        <div className={`preLoader ${!initialize ? '' : 'initialize'}`} />
    );
};

export default preLoader;