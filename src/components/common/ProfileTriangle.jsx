import React from 'react';

const ProfileTriangle = (props) => {
    return (
        <svg
        {...props} 
        version="1.1"
        id="Capa_1"
        xmlns="http://www.w3.org/2000/svg"
        xlink="http://www.w3.org/1999/xlink"
        x="0px"
        y="0px"
        width="14px"
        height="14px"
        viewBox="0 0 552.611 552.611"
        space="preserve"
      >
        <g>
          <g>
            <path
              d="M486.413,221.412L122.347,12.916c-52.938-30.318-95.852-5.44-95.852,55.563v415.652c0,61.004,42.914,85.882,95.852,55.563
        l364.066-208.49C539.351,300.887,539.351,251.731,486.413,221.412z"
            />
          </g>
        </g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
        <g></g>
      </svg>
    );
};

export default ProfileTriangle;