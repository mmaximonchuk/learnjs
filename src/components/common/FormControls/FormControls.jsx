import React from "react";
import "./form-controls.scss";

const FormControl = ({ input, meta: {touched, error, visited}, ...props }) => {
  const hasError = error && visited && touched;
  return (
    <div className={`form-control ${hasError ? "error" : ""}`}>
      {props.children}
      {hasError && <div className={`form__details-info ${input.name === 'password' ? 'password' : ''}`}>
        <div className="triangle" />
        <span>{`${hasError ? error : ""}`}</span>
      </div>}
    </div>
  );
};

export const Textarea = (props) => {
    const { input, meta, ...restProps } = props;
  return (
    <FormControl {...props}>
      <textarea {...restProps} {...input} />
    </FormControl>
  );
};
export const Input = (props) => {
    const { input, meta, ...restProps } = props;
    return (
    <FormControl {...props}>
      <input {...restProps} {...input} />
    </FormControl>
  );
};
