import React, { useState, useEffect } from "react";

const Pagination = ({
  pageSize,
  totalUsersCount,
  changePageHandler,
  currentPage,
}) => {
  const pages = [];
  const pagesCount = Math.ceil(totalUsersCount / pageSize);
  for (let i = 1; i <= pagesCount; i++) {
    pages.push(i);
  }

  const [arrayOfCurrentPages, setArrayOfCurrentPages] = useState(pages);

  useEffect(() => {
    let tempNumberOfPages = [...arrayOfCurrentPages];

    let dotsInitial = '...';
    let dotsLeft = '... ';
    let dotsRight = ' ...';

    if (currentPage >= 1 && currentPage <= 3) {
      tempNumberOfPages = [1, 2, 3, 4, dotsInitial, pages.length];
    } else if (currentPage === 4) {
      const sliced = pages.slice(0, 5);
      tempNumberOfPages = [...sliced, dotsInitial, pages.length];
    } else if (currentPage > 4 && currentPage < pages.length - 2) {
      const sliced1 = pages.slice(currentPage - 2, currentPage);
      const sliced2 = pages.slice(currentPage, currentPage + 1); 
      tempNumberOfPages = [
        1,
        dotsLeft,
        ...sliced1,
        ...sliced2,
        dotsRight,
        pages.length,
      ];
    } else if (currentPage > pages.length - 3) {
        const sliced = pages.slice(currentPage - 2, currentPage + 2);
        tempNumberOfPages = ([1, dotsLeft, ...sliced]);
    } else if (currentPage === dotsInitial) {
        changePageHandler(arrayOfCurrentPages[arrayOfCurrentPages.length - 3] + 1);
    } else if (currentPage === dotsRight) {
        changePageHandler(arrayOfCurrentPages[3] + 2);
    } else if (currentPage === dotsLeft) {
        changePageHandler(arrayOfCurrentPages[3] - 2);
    }

    setArrayOfCurrentPages(tempNumberOfPages);
  }, [currentPage]);

  return (
    <div className="pagination-wrapper">
      {arrayOfCurrentPages.map((page) => {
        return (
          <button
            key={page}
            onClick={() => changePageHandler(page)}
            className={`pagination-number ${
              currentPage === page ? "current" : ""
            }`}
          >
            {page}
          </button>
        );
      })}
    </div>
  );
};

export default Pagination;
