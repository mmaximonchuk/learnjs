import React from "react";
import { useEffect } from "react";
import { connect } from "react-redux";
import { compose } from "redux";
// import withAuthRedirect from "../../HOC/WithAuthRedirect";
import {
  changeCurrentPage,
  toggleFollowUserThunk,
  setNumberOfPages,
  getFriendsAndUsersThunk,
} from "../../redux/findFriendsAndUsersReducer";
import { getArrayOfCurrentPagesState, getFollowingProgressState, getPageSizeState, getUsersState, getTotalUsersCountState, getCurrentPageState, getIsLoadingState } from "../../redux/users-selectors";
import FriendsAndUsersLayout from "./FriendsAndUsersLayout";

const FriendsAndUsersContainer = (props) => {
  const setPaginator = (currentPage, totalUserCount, pageSize) => {
    let pages = [];
    let pagesCount = Math.ceil(totalUserCount / pageSize);
    for (let i = 1; i <= pagesCount; i++) {
      pages.push(i);
    }
    //  props.setNumberOfPages(pages);

    let tempNumberOfPages = [...pages];
    let dotsInitial = "...";
    let dotsLeft = "... ";
    let dotsRight = " ...";

    if (currentPage >= 1 && currentPage <= 3) {
      tempNumberOfPages = [1, 2, 3, 4, dotsInitial, pages.length];
    }
    if (currentPage === 4) {
      const sliced = pages.slice(0, 5);
      tempNumberOfPages = [...sliced, dotsInitial, pages.length];
    }
    if (currentPage > 4 && currentPage < pages.length - 2) {
      const sliced1 = pages.slice(currentPage - 2, currentPage);
      const sliced2 = pages.slice(currentPage, currentPage + 1);
      tempNumberOfPages = [
        1,
        dotsLeft,
        ...sliced1,
        ...sliced2,
        dotsRight,
        pages.length,
      ];
    }
    if (currentPage > pages.length - 3) {
      const sliced = pages.slice(currentPage - 2, currentPage + 2);
      tempNumberOfPages = [1, dotsLeft, ...sliced];
    }
    if (currentPage === dotsInitial) {
      changePageHandler(
        props.arrayOfCurrentPages[props.arrayOfCurrentPages.length - 3] + 1
      );
      const sliced1 = props.arrayOfCurrentPages.slice(currentPage - 2, currentPage);
      const sliced2 = props.arrayOfCurrentPages.slice(currentPage, currentPage + 1);

      tempNumberOfPages = [
        1,
        dotsLeft,
        ...sliced1,
        ...sliced2,
        dotsRight,
        pages.length,
      ];
    }
    if (currentPage === dotsRight) {
      changePageHandler(props.arrayOfCurrentPages[3] + 2);
      const sliced1 = props.arrayOfCurrentPages.slice(currentPage - 2, currentPage);
      const sliced2 = props.arrayOfCurrentPages.slice(currentPage, currentPage + 1);

      tempNumberOfPages = [
        1,
        dotsLeft,
        ...sliced1,
        ...sliced2,
        dotsRight,
        pages.length,
      ];
    }
    if (currentPage === dotsLeft) {
      changePageHandler(props.arrayOfCurrentPages[3] - 2);
      const sliced1 = props.arrayOfCurrentPages.slice(currentPage - 2, currentPage);
      const sliced2 = props.arrayOfCurrentPages.slice(currentPage, currentPage + 1);

      tempNumberOfPages = [
        1,
        dotsLeft,
        ...sliced1,
        ...sliced2,
        dotsRight,
        pages.length,
      ];
    }
    props.setNumberOfPages(tempNumberOfPages);
  };


  useEffect(() => {
    props.getFriendsAndUsersThunk(
      props.currentPage,
      props.pageSize,
      setPaginator
    );
  }, [])

  const changePageHandler = (pageNumber) => {
    props.getFriendsAndUsersThunk(
      pageNumber,
      props.pageSize,
      setPaginator
    );
  };

  const toggleFollowHandler = (user) => {
    props.toggleFollowUserThunk(user);
  }

  return (
      <>
        {/* {props.isLoading && <div class="preLoader" />} */}
        <FriendsAndUsersLayout
          totalUsersCount={props.totalUsersCount}
          pageSize={props.pageSize}
          currentPage={props.currentPage}
          changePageHandler={changePageHandler}
          toggleFollowHandler={toggleFollowHandler}
          friendsAndUsers={props.friendsAndUsers}
          isLoading={props.isLoading}
          followingProgress={props.followingProgress}
          arrayOfCurrentPages={props.arrayOfCurrentPages}
        />
      </>
    );
}


const mapStateToProps = (state) => {
  return {
    friendsAndUsers: getUsersState(state),
    pageSize: getPageSizeState(state),
    totalUsersCount: getTotalUsersCountState(state),
    currentPage: getCurrentPageState(state),
    isLoading: getIsLoadingState(state),
    followingProgress: getFollowingProgressState(state),
    arrayOfCurrentPages: getArrayOfCurrentPagesState(state),
  };
};
// const mapStateToProps = (state) => {
//   return {
//     friendsAndUsers: state.findFriendsandUsersPage.friendsAndUsers,
//     pageSize: state.findFriendsandUsersPage.pageSize,
//     totalUsersCount: state.findFriendsandUsersPage.totalUsersCount,
//     currentPage: state.findFriendsandUsersPage.currentPage,
//     isLoading: state.findFriendsandUsersPage.isLoading,
//     followingProgress: state.findFriendsandUsersPage.followingProgress,
//     arrayOfCurrentPages: state.findFriendsandUsersPage.arrayOfCurrentPages,
//   };
// };

// const mapDispatchToProps = (dispatch) => {
//   return {
//     toggleFollow: (userId) => {
//       dispatch(toggleFollow(userId));
//     },
//     setFriendsAndUsers: (friendsAndUsers) => {
//       dispatch(setFriendsAndUsers(friendsAndUsers));
//     },
//     changeCurrentPage: (pageNumber) => {
//       dispatch(changeCurrentPage(pageNumber));
//     },
//     setTotalUsersCount: (usersCount) => {
//       dispatch(setTotalUsersCount(usersCount));
//     },
//     setLoadingState: (isLoading) => {
//       dispatch(setLoadingState(isLoading));
//     },
//     setNumberOfPages: (pages) => {
//       dispatch(setNumberOfPages(pages));
//     },
//   };
// };

// export default connect(mapStateToProps, {
//   changeCurrentPage,
//   setNumberOfPages,
//   getFriendsAndUsersThunk,
//   toggleFollowUserThunk,
// })(FriendsAndUsersContainer);

export default compose(
  connect(mapStateToProps, {
    changeCurrentPage,
    setNumberOfPages,
    getFriendsAndUsersThunk,
    toggleFollowUserThunk,
  }),
  // withAuthRedirect
)(FriendsAndUsersContainer);
