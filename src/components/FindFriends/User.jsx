import React from 'react';
import { NavLink } from "react-router-dom";
import userImg from "../../images/dialogs/avatars/template.jfif";

const User = (user, followingProgress, toggleFollowHandler, setFollowingProgress) => {

    return (
        <div className="user">
              <div className="left">
                <NavLink to={`/profile/${user.user.id}`}>
                  <img
                    className="user-avatar"
                    src={user.user.photos.small || userImg}
                    alt="user-avatar"
                  />
                </NavLink>
                <button
                  disabled={user.followingProgress.some((id) => id === user.user.id)}
                  onClick={() =>
                    user.toggleFollowHandler(user.user, user.setFollowingProgress)
                  }
                  className="follow-btn"
                >
                  {user.user.followed ? "unfollow" : "follow"}
                </button>
              </div>
              <div className="right">
                <span className="user-full-name">{user.user.name}</span>
                <span className="user-status">{user.user.status}</span>
                <div className="location">
                  <span className="city">{/*user.location.city*/}Kyiv</span>
                  <span className="country">
                    {/*user.location.country*/}Ukraine
                  </span>
                </div>
              </div>
            </div>
    );
};

export default User;