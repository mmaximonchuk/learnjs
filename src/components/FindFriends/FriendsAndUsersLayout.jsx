import React from "react";
import PreLoader from "../common/PreLoader";
import "../FindFriends/FindFriends.css";
import User from "./User";
// import Pagination from "../common/Pagination";

const FriendsAndUsersLayout = (props) => {
  const pages = props.arrayOfCurrentPages;
  return (
    <div className="find-friends">
      <h2 className="find-friends__title">Friend & Users</h2>
      {/* Pagination */}
      <div className="pagination-wrapper">
        {pages.map((page) => {
          return (
            <button
              key={page}
              onClick={() => props.changePageHandler(page)}
              className={`pagination-number ${
                props.currentPage === page ? "current" : ""
              }`}
            >
              {page}
            </button>
          );
        })}
      </div>
      {/* Pagination */}
      {props.isLoading ? (
        <PreLoader />
      ) : (
        props.friendsAndUsers.map((user) => {
          return (
            <User
              key={user.id}
              user={user}
              toggleFollowHandler={props.toggleFollowHandler}
              followingProgress={props.followingProgress}
              setFollowingProgress={props.setFollowingProgress}
            />
          );
        })
      )}
      <button className="show-more-btn">Show More</button>
    </div>
  );
};

export default FriendsAndUsersLayout;
