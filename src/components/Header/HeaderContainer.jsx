import React from "react";
import { connect } from "react-redux";
import { compose } from "redux";
import { logOutThunk } from "../../redux/auth-reducer";
import Header from "./Header";

const HeaderContainer = ({ logOutThunk, ...restProps }) => {
  const logOutHandler = () => logOutThunk();

  return <Header logOutHandler={logOutHandler} {...restProps} />;
};

const mapStateToProps = (state) => {
  return {
    id: state.auth.id,
    email: state.auth.email,
    login: state.auth.login,
    isLoading: state.auth.isLoading,
  };
};

export default compose(
  connect(mapStateToProps, {
    logOutThunk,
  })
)(HeaderContainer);
