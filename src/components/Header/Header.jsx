import React from "react";
import "./header.css";
import { NavLink } from "react-router-dom";

import iconLogo from "../../images/medium.svg";
// import PreLoader from "../common/PreLoader";
// import imgLogOut from "../../images/log-out.svg";

const Header = ({login, logOutHandler}) => {
  return (
    <header className="header">
      <div className="header__container">
        <NavLink to="/profile" className="header__logo">
          <img src={iconLogo} alt="logo" />
        </NavLink>
        <div className="header__auth">
          {!login ? (
            <>
              {/* <PreLoader /> */}
              <NavLink to="/login" className="auth__btn">
                Log In
              </NavLink>
            </>
          ) : (
            <NavLink className="auth__name" to="/profile">
              <img
                src="https://i.pinimg.com/originals/7c/c7/a6/7cc7a630624d20f7797cb4c8e93c09c1.png"
                alt="Person avatar"
                className="auth__avatar"
              />
              <span>Hi, {login}</span>
              <button className="auth__log-out-btn" onClick={logOutHandler}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="-5 -3 24 24"
                width="24"
                height="24"
                preserveAspectRatio="xMinYMin"
                className="auth__log-out"
                
              >
                <path d="M3.414 7.828h5.642a1 1 0 1 1 0 2H3.414l1.122 1.122a1 1 0 1 1-1.415 1.414L.293 9.536a.997.997 0 0 1 0-1.415L3.12 5.293a1 1 0 0 1 1.415 1.414L3.414 7.828zM13 0a1 1 0 0 1 1 1v16a1 1 0 0 1-2 0V1a1 1 0 0 1 1-1z"></path>
              </svg>
              </button>
            </NavLink>
          )}
        </div>
      </div>
    </header>
  );
};

export default Header;
