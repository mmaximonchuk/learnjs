import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";


let mapStateToPropsForRedirect = (state) => {
  return {
    isAuth: state.auth.isAuth,
  };
};

const WithAuthRedirect = (Component) => {

  const RedirectComponent = (props) => {
    const [forbidRender, setForbidRender] = useState(props.isAuth);
    
  useEffect(() => {
    if(!forbidRender) {
      setForbidRender(true)
    }
  }, [forbidRender]);

    return forbidRender ? <Component {...props} /> : <Redirect to={"/login"} /> 
  }

  const ConnectedAuthRedirectComponent = connect(mapStateToPropsForRedirect)(RedirectComponent);

  return ConnectedAuthRedirectComponent;
};



export default WithAuthRedirect;
