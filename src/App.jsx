import React from "react";
import Navbar from "./components/Navbar/Navbar";
import ProfileContainer from "./components/Profile/ProfileContainer";
import DialogsContainer from "./components/Dialogs/DialogsContainer";
import FindFriendsContainer from "./components/FindFriends/FindFriendsContainer";
import News from "./components/News/News";
import Music from "./components/Music/Music";
import Settings from "./components/Settings/Settings";
import { Route, withRouter } from "react-router-dom";
import { initializeApp } from "./redux/app-reducer";
import HeaderContainer from "./components/Header/HeaderContainer";
import Login from "./components/Login/Login";
import { connect } from "react-redux";

import "./index.css";
import { compose } from "redux";
import PreLoader from "./components/common/PreLoader";
class App extends React.Component {

  componentDidMount() {
    this.props.initializeApp();
  }

  render() {
    if(!this.props.isInitialized) return <PreLoader initialize />

    return (
      <div className="app-wrapper">
        <HeaderContainer />
        <Navbar />
        <div className="app-wrapper-content">
          <Route path="/profile/:userId?" render={() => <ProfileContainer />} />
          <Route path="/dialogs" render={() => <DialogsContainer />} />
          <Route path="/news" render={() => <News />} />
          <Route path="/music" render={() => <Music />} />
          <Route path="/login" render={() => <Login />} />

          <Route
            path="/friends-and-users"
            render={() => <FindFriendsContainer />}
          />
          <Route path="/settings" render={() => <Settings />} />
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => ({
  isInitialized: state.app.isInitialized
})
export default compose(
  withRouter,
  connect(mapStateToProps, {
    initializeApp,
  })
)(App);
